#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from setuptools import find_namespace_packages, setup

assert sys.version_info[0] >= 3

with open("README.md") as readme_file:
    readme = readme_file.read()

with open('VERSION.txt') as version_file:
    version = version_file.read().strip()

with open('jones_solvers/version.py', 'w') as version_file:
    version_file.write(f'''
# This is an automatically-generated file
# DO NOT EDIT

_version = '{version}'
'''.strip())


setup(
    name="ska-sdp-jones-solvers",
    version=version,
    python_requires=">=3.0",
    description="Jones Solver Library",
    long_description="A reference library of python-based antenna Jones matrix solvers",
    author="Daniel Mitchell",
    author_email="daniel.mitchell@csiro.au",
    url="https://gitlab.com/ska-telescope/sdp/ska-sdp-jones-solvers.git",
    packages=find_namespace_packages(exclude=['tests']),
    license="UWA and CSIRO Open Source Software Licence Agreement",
    zip_safe=False,
    classifiers=[
        "Development Status :: Beta",
        "Intended Audience :: Developers",
        "License :: variation of the BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
    ],
    # install_requires=reqs,
    #scripts=["bin/get_rascil_data"],
    test_suite="tests",
    tests_require=["pytest",
                   "pytest-bdd",
                   "pytest-cov",
                   "pycodestyle"],
    install_requires=["numpy",
                      "astropy >= 4.3",
                      "python-casacore >= 3.1.1",
                      "rascil"],
)

