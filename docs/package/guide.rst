.. doctest-skip-all
.. _package-guide:

.. todo::
    - Fix up linting
    - Time and frequency calibration intervals?
    - Add single-polarisation gain solvers for comparison?
    - Add pre-averaging for algorithm 1?
    - Use names rather than indices for algorithms?
    - Change name of the python package from jones_solvers to
      ska-sdp-jones-solvers?

***************************
Jones Solvers Documentation
***************************

Solvers
=======

At present, a single set of matrices is found for all times and frequencies,
as might be required for real-time calibration. For more general-purpose
operation the solutions will need to be split into time and frequency
intervals.

RTS-Style Solver
----------------

Algorithm 1 of ``solve_jones()`` iteratively updates the set of Jones matrices,
but within each iteration performs an independent least-squares optimisation
for each matrix without considering how the others are updating. This is
equivalent to forming a normal matrix and setting the off-diagonal terms to
zero. While it does not converge as fast as the normal-equation approach, it
scales well in terms of operations and memory, and allows the free parameter
for each antenna/station to be the 2x2 Jones matrix. It is based on the
equivalent solver in the MWA RealTime System (Mitchell et at., 2008, IEEE
JSTSP, 2, JSTSP.2008.2005327).

The equation to be minimised is similar to the `scalar equation from RASCIL
<https://ska-telescope.gitlab.io/external/rascil/processing_components/calibration/index.html>`_,
but with 2x2 Jones matrices, `J`, instead of scalar gain terms and 2x2
coherency matrices, `V` instead of scalar visibilties: 

.. math:: S = \sum_{t,f}^{}{\sum_{i,j}^{}{w_{t,f,i,j}\left|\left| V_{t,f,i,j}^{\text{obs}} - J_{i}V_{t,f,i,j}^{\text{mod}}{J_{j}^{*}} \right|\right|}^{2}_{F}}

where :math:`||A||^{2}_{F}` the squared Frobenius norm of a matrix A, equal to
the trace of :math:`AA^H`.

Yandasoft-Style Solver
----------------------

Algorithm 2 of ``solve_jones()`` is a full normal-equation based linear
least-squares algorithm. It is based on the approach in Yandasoft. Normal
equations can be generated via initial creation of a design matrix, which may
be more efficient when calibrating on short time and frequency intervals, or
normal-equation element products can be accumulated directly, allowing for an
initial accumulation over time and frequency. In Yandasoft the latter was
implemented by Max Voronkov using an automatic differentiation class for
complex data and parameters. See
`Max's CalIm presentation <https://indico.skatelescope.org/event/171/contributions/1007/>`_
for details.

.. Automatic API Documentation section. Generating the API from the docstrings. Modify / change the directory structure as you like

Public API Documentation
````````````````````````
Functions
---------

.. automodule:: jones_solvers.processing_components.solve_jones
    :members:

..
 Classes
 -------
 .. autoclass:: jones_solvers.processing_components.solve_jones
     :noindex:
     :members:
