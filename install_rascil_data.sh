#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: $0 <rascil-data-dir>" 1>&2
    exit 1
fi

target_dir="$1"

if [ -d "$target_dir" ]; then
    exit 0
fi

mkdir -p "$target_dir"
cd "$target_dir"

if ! command -v wget &> /dev/null; then
    apt-get -y update
    apt-get install -y wget
fi

wget -O rascil_data.tar.gz https://ska-telescope.gitlab.io/external/rascil/rascil_data.tgz
tar zxf rascil_data.tar.gz
